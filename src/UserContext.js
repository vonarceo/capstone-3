import React from 'react'

 //core function of react / initialize a react context 
const UserContext = React.createContext()

// initializes a context provider and give us ability to provide a specific context through a component


export const UserProvider = UserContext.Provider

export default UserContext