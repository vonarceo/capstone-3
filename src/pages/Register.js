import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import UserContext from '../UserContext'
import { useNavigate, Navigate } from 'react-router-dom'
import Swal from 'sweetalert2'
import { Row, Col } from 'react-bootstrap'

export default function Register(){

	const {user} = useContext(UserContext)

	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNo, setMobileNo] = useState('')

	const navigate = useNavigate()

	//for determining if button is disabled or not
	const [isActive, setIsActive ] = useState(false)

	function registerUser(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/check-email`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
				


			})
		})		
		.then(response => response.json())
		.then(result => {

			
				if(result === true){
				Swal.fire({
					title: 'Opps!',
					icon: 'error',
					text: 'Email already exist'
				})
			}	
			else {
				// register request
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
						


					})
				})
				.then(response => response.json())
				.then(result => {
					if(!result){
						Swal.fire({
							title: 'Something went wrong!',
							icon: 'error',
							text: 'please try again :('
						})
					}
					else {

						Swal.fire({
							title: 'Congrats!',
							icon: 'success',
							text: 'registered successful'
						})

						setEmail('')
						setPassword1('')
						setPassword2('')
						setFirstName('')
						setLastName('')
						setMobileNo('')
						navigate('/login')
					}
				})


			}
					
		})



		// Clear out the input fields after submitting
		

		
		
	}


	useEffect(() =>{
		if((firstName !== '' && lastName !== '' && mobileNo.length === 11 && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			//enable the button
			setIsActive(true)
		}

		else {
			setIsActive(false)
		}

	}, [firstName, lastName, mobileNo, email, password1, password2])


	return(

		(user.id != null) ?
		<Navigate to="/"/>

		:
		<Row className="mt-3 mb-3 justify-content-center">
		 	<Col md="4" >
				<Form onSubmit={event => registerUser(event)}>
		    
				<Form.Group controlId="firstName">
			        <Form.Label>First Name</Form.Label>
			        <Form.Control 
			            type="text" 
			            placeholder="Enter first name"
			            value={firstName}
			            onChange={event => setFirstName(event.target.value)} 
			            required
			        />  
			    </Form.Group>

			    <Form.Group controlId="lastName">
			        <Form.Label>Last Name</Form.Label>
			        <Form.Control 
			            type="text" 
			            placeholder="Enter last name"
			            value={lastName}
			            onChange={event => setLastName(event.target.value)} 
			            required
			        />  
			    </Form.Group>

			    <Form.Group controlId="mobileNo">
			        <Form.Label>Mobile Number</Form.Label>
			        <Form.Control 
			            type="text" 
			            placeholder="Enter mobile number"
			            value={mobileNo}
			            onChange={event => setMobileNo(event.target.value)} 
			            required
			        />  
			    </Form.Group>


			    <Form.Group controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control 
			            type="email" 
			            placeholder="Enter email"
			            value={email}
			            onChange={event => setEmail(event.target.value)} 
			            required
			        />
			        <Form.Text className="text-muted">
			            We'll never share your email with anyone else.
			        </Form.Text>
			    </Form.Group>

			    <Form.Group controlId="password1">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
			            type="password" 
			            placeholder="Password"
			            value={password1}
			            onChange={event => setPassword1(event.target.value)} 
			            required
			        />
			    </Form.Group>

			    <Form.Group controlId="password2">
			        <Form.Label>Verify Password</Form.Label>
			        <Form.Control 
			            type="password" 
			            placeholder="Verify Password"
			            value={password2}
			            onChange={event => setPassword2(event.target.value)} 
			            required
			        />
			    </Form.Group>
			    	<br/>

			    	{	isActive ?
			    		<Button variant="primary" type="submit" id="submitBtn">
			    			Submit
			    		</Button>
			    		:
			    		<Button variant="danger" type="submit" id="submitBtn" disabled>
			    			Submit
			    		</Button>
			    	}
		    			   
				</Form>
			</Col>	
		</Row>
		
	)
}