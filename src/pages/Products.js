import ProductCard from '../components/ProductCard'

import {useEffect, useState} from 'react'
//import courses_data from '../data/courses' - not needed
import Loading from '../components/Loading'

export default function Products(){

	const [isLoading, setIsLoading] = useState(false)
	
	const [products, setProducts] = useState([])

	useEffect(() => {
		// set the loading state to true 
			setIsLoading(true)

		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(response => response.json())
		.then(result => {
				setProducts(
					result.map(product => {
							
							return(
								<>	
								<ProductCard key={product._id} product={product}/>	
									
								</>
							)
							
						})
					)
				setIsLoading(false)
		})
	}, [])

	return(
			(isLoading) ?
			  
			 		<Loading/>
			 	:	
				<>
			 
					{products}		
				</>
	)
}

