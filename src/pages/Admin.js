import ProductCard from '../components/ProductCard'
import AdminDashboard from '../components/AdminDashboard'
import Loading from '../components/Loading'

import {useEffect, useState} from 'react'
//import courses_data from '../data/courses' - not needed


export default function Products(){

	const [isLoading, setIsLoading] = useState(false)
	
	// const [products, setProducts] = useState([])



	// useEffect(() => {
	// 	// set the loading state to true 
	// 		setIsLoading(true)

	// 	fetch(`${process.env.REACT_APP_API_URL}/products/all`)
	// 	.then(response => response.json())
	// 	.then(result => {
	// 			setProducts(
	// 				result.map(product => {
								
	// 						return(
	// 							<>	
	// 							<AdminDashboard key={product._id} product={product}/>	
									
	// 							</>
	// 						)
							
	// 					})
	// 				)
	// 			setIsLoading(false)
	// 	})
	// }, [])

	return(
			(isLoading) ?
			  
			 		<Loading/>
			 	:	
				<>
			 
					{/*{products}	*/}
					<AdminDashboard/>						
				</>

	)
}