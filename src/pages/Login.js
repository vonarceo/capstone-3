import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import { Row, Col } from 'react-bootstrap'

export default function Login(){
	// Initializes the use of the properties from the userProvider in App.js 
	const {user, setUser} = useContext(UserContext)
	
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	//initialize useNavigate
	const navigate = useNavigate()

	const [isActive, setIsActive ] = useState(false)



	const retrieveUser = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(result => {
			

			// store the user details retreive from the token into the global user state
			setUser({
				id: result._id,
				isAdmin: result.isAdmin
			})
		})
	} 

	function authenticate(event){
		event.preventDefault()
		
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				
				email: email,
				password: password

			})
		})
		.then(response => response.json())
		.then(result => {
			if(typeof result.accessToken !== "undefined"){
				localStorage.setItem('token', result.accessToken)

				retrieveUser(result.accessToken)
				Swal.fire({
				  icon: 'success',
				  title: 'Login Successful',
				  text: 'Welcome to my e-shop!'
				})
			} else {
				Swal.fire({
				  icon: 'error',
				  title: 'Oops...',
				  text: 'Wrong Email or Password.',
				  footer: '<a href="https://support.google.com/accounts/answer/6009563?hl=en">Why do I have this issue?</a>'
				})
			}
		})
		
		
	}


	


	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	}, [email, password])



	return(
				(user.id != null) ?
				
			<Navigate to="/"/>
			:
			<Row className="mt-3 mb-3 justify-content-center">
				<Col md="4">
			<Form onSubmit={event => authenticate(event)}>
				<Form.Group controlId="userEmail">
				    <Form.Label>Email address</Form.Label>
				    <Form.Control 
				        type="email" 
				        placeholder="Enter email"
				        value={email}
				        onChange={event => setEmail(event.target.value)} 
				        required
				    />
				    <Form.Text className="text-muted">
				        We'll never share your email with anyone else.
				    </Form.Text>
				</Form.Group>
				<Form.Group controlId="password">
				    <Form.Label>Password</Form.Label>
				    <Form.Control 
				        type="password" 
				        placeholder="Password"
				        value={password}
				        onChange={event => setPassword(event.target.value)} 
				        required
				    />
				</Form.Group>
				
				<br/>

				{ isActive ?
				<Button variant="danger" type="submit" id="submitBtn">
					Submit
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" disabled>
					Submit
				</Button>
				}
			</Form>
				</Col>
			</Row>
	)
}