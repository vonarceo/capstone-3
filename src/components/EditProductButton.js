import {useState, useEffect} from 'react'
import { Button, Modal, Form } from 'react-bootstrap'
import Swal from 'sweetalert2'

 export default function EditProductButton(product){

	const [updateName, setUpdateName] = useState(product.product.name)
	const [updateDescription, setUpdateDescription] = useState(product.product.description)
	const [updatePrice, setUpdatePrice] = useState(product.product.price)
	const [updateProductId, setUpdateProductId] = useState(product.product._id)

	const [show2, setShow2] = useState(false);
	const handleClose2 = () => setShow2(false);
	const handleShow2 = () => setShow2(true);

		
	
	function updateProduct(event){
		event.preventDefault()
	  fetch(`${process.env.REACT_APP_API_URL}/products/${product.product._id}/update`, {
	    method: 'PATCH',
	    headers: {
	      'Content-Type': 'application/json'
	    },
	    body: JSON.stringify({
	      name: updateName,
	      description: updateDescription,
	      price: updatePrice
	      
	    })
	  })
	  .then(response => response.json())
	  .then(result => {
	    if(result){
	      Swal.fire({
	        title: 'Success!',
	        icon: 'success',
	        text: 'Successfully added new product!'
	      })
	      
	    }
	    else {
	      Swal.fire({
	        title: 'Something went wrong',
	        icon: 'error',
	        text: 'please contact developer.'
	      })

	      

	    }
	  })
	}



	return(
		<>
		<Button variant="secondary" onClick={handleShow2}>Update</Button>

		<Modal show={show2} onHide={handleClose2}>
		    <Modal.Header closeButton>
		      <Modal.Title>Update Products</Modal.Title>
		    </Modal.Header>
		    
		    <Modal.Body>
		    
		      
		      <Form onSubmit={event => updateProduct(event)}>

		        <Form.Group className="mb-1" controlId="name">
		          <Form.Label>Product Name:</Form.Label>
		          <Form.Control
		            type="text"
		            placeholder="Enter Product Name"
		            value={updateName}
		            
		            onChange={event => setUpdateName(event.target.value)}
		            required
		          
		          />
		        </Form.Group>
		        

		        <Form.Group
		          className="mb-1"
		          controlId="description"
		        >
		          <Form.Label>Description:</Form.Label>
		          <Form.Control 
		            type="text"
		            placeholder="Enter Description"
		            value={updateDescription}
		            
		            onChange={event => setUpdateDescription(event.target.value)}
		            required    
		          />
		        </Form.Group>

		        <Form.Group
		          className="mb-1"
		          controlId="price"
		        >
		          <Form.Label>Price:</Form.Label>
		          <Form.Control 
		            type="text"
		            placeholder="Enter Price"
		            value={updatePrice}
		            
		            onChange={event => setUpdatePrice(event.target.value)}
		            required    
		          />
		        </Form.Group>

		        <Button variant="danger" type="submit" onClick={handleClose2} id="submitBtn">
		            Update
		          </Button>

		      </Form>
		      

		    </Modal.Body>
		    
		  </Modal>
		  </>
	)
}