import {Button, Row, Col, Carousel} from 'react-bootstrap'
import { Link } from 'react-router-dom'


export default function	Banner(){
	return(
		<Row>
			<Col className="p-5 text-center" >
				<h1>Hype's Collection</h1>
				<p>Everything you need!</p>
				
				<Link className="btn btn-danger" to={`/products`}>View Collections</Link>


			</Col>
		</Row>
	)
}

