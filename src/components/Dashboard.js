import { Row, Col, Button, Modal, Form } from 'react-bootstrap'
import Admin from '../pages/Admin'
import { Link } from 'react-router-dom'
import { useState, useEffect } from 'react'
import Swal from 'sweetalert2'

export default function Dashboard(){

	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
 	const handleShow = () => setShow(true);	

 	const [show2, setShow2] = useState(false);
	const handleClose2 = () => setShow2(false);
 	const handleShow2 = () => setShow2(true);	

 	const [name, setName] = useState('')
 	const [description, setDescription] = useState('')
 	const [price, setPrice] = useState('')
 	const [isActive, setIsActive ] = useState(false)

	function addProduct(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/create`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
				
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result){
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Successfully added new product!'
				})
				
			}
			else {
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'please contact developer.'
				})

				

			}
		})		
			
	}

	// useEffect(() =>{
	// 	if(name !== null && description !== null){
			
	// 		setIsActive(false)
	// 	}

	// 	else {
	// 		setIsActive(true)
	// 	}

	// }, [name, description, price])



	return(

		<>

		<Row>
			<Col className="p-5 text-center" >
				<h1>Admin Dashboard</h1>
					<>
					  <Button variant="danger" onClick={handleShow}>
					    Add Product
					  </Button>

					  {/*<Button variant="secondary" onClick={handleShow2}>
					    Show Orders
					  </Button>*/}

					  <Modal show={show} onHide={handleClose}>
						    <Modal.Header closeButton>
						      <Modal.Title>Add new product</Modal.Title>
						    </Modal.Header>
						    
						    <Modal.Body>
						      
						      <Form onSubmit={event => addProduct(event)}>

						        <Form.Group className="mb-1" controlId="name">
						          <Form.Label>Product Name:</Form.Label>
						          <Form.Control
						            type="text"
						            placeholder="Enter Product Name"
						            value={name}
						            
						            onChange={event => setName(event.target.value)}
						            required
						          
						          />
						        </Form.Group>
						        

						        <Form.Group
						          className="mb-1"
						          controlId="description"
						        >
						          <Form.Label>Description:</Form.Label>
						          <Form.Control 
						          	type="text"
						          	placeholder="Enter Description"
						          	value={description}
						          	
						          	onChange={event => setDescription(event.target.value)}
						          	required		
						          />
						        </Form.Group>

						        <Form.Group
						          className="mb-1"
						          controlId="price"
						        >
						          <Form.Label>Price:</Form.Label>
						          <Form.Control 
						          	type="text"
						          	placeholder="Enter Price"
						          	value={price}
						          	
						          	onChange={event => setPrice(event.target.value)}
						          	required		
						          />
						        </Form.Group>

						      <Button variant="danger" type="submit" id="submitBtn" onClick={handleClose}>
						      		Add Product
						      	</Button>

						      </Form>

						    </Modal.Body>
						    <Modal.Footer>
						      
						    	<p>--for additional content</p>

						    </Modal.Footer>
						  </Modal>
						</>		
						
					  {/*<Modal show={show2} onHide={handleClose2}>
						    <Modal.Header closeButton>
						      <Modal.Title>Orders</Modal.Title>
						    </Modal.Header>
						    
						    <Modal.Body>
						    
						      
						      

						    </Modal.Body>
						    
						    <Modal.Footer>
						      
						    	<Button variant="secondary" onClick={handleClose2}>
						                  Close
						                </Button>
						       <Button variant="primary" onClick={handleClose2}>
						                  Save Changes
						                </Button>

						    </Modal.Footer>
						  </Modal>	*/}
			</Col>
		</Row>


	
		
	<Admin/>		
		</>
		
		)
}