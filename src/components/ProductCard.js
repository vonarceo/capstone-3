import {useState, useEffect} from 'react'
import { Row, Col, Card, Container } from 'react-bootstrap'
import { Button, Table } from 'react-bootstrap'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

export default function ProductCard({product}){
	
	// Destructuring the property
	const {name, description, price, _id, isActive} = product

	// Using the state // initialize the value of count to 0
		// count is variable, setCount is to set the value, useState = starting
		// const [count, setCount] = useState(0)
		// const [slot, setSlot] = useState(15)
		// const [isOpen, setIsOpen] = useState(true)


		// function enroll(){
		// 	if(slot > 0){

		// 	setCount(count + 1)
		// 	setSlot(slot -1)
		// 	}

		// 	else {
		// 		alert('Slots are full')
		// 	}

		// }

		// effects in react is just like side effect in real life where everytime something happen within component, a function or condition run

		// you may also listen or watch specific state for changes instead of watching/listening to the whole component
		// useEffect(() => {
		// 	if(slot === 0){
		// 		setIsOpen(false)
		// 	}
		// }, [slot])



	return(
		
		<Row className="mb-3 mt-3 justify-content-center text-center">
			<Col md="4">
				<Card>
					<Card.Body>
						<Card.Title>
							<h3>
								{name}
							</h3>
							
						</Card.Title>
						<Card.Text>
							{description}
						</Card.Text>
						<Card.Title>₱{price}</Card.Title>
						
						<Link className="btn btn-secondary" to={`/products/${_id}`}>Details</Link>
						</Card.Body>
				</Card>

			
			</Col>
		</Row>
		
	)
}


// Prop Types can be use to validate the data coming from the props. you can define each property of the props and assign specific validation for each of them.

ProductCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}

		

