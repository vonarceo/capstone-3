import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import { Link, NavLink } from 'react-router-dom'
import { useContext } from 'react'
import UserContext from '../UserContext'
import NavDropdown from 'react-bootstrap/NavDropdown';




export default function AppNavbar(){

	const {user} = useContext(UserContext)

	return(
		<Navbar sticky="top" bg="light" expand="lg">
			<Navbar.Brand as={Link} to="/">#vonarceo</Navbar.Brand>
			
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="navcolor ms-auto">

					<Nav.Link as={NavLink} to="/">Home</Nav.Link>
					
					
					
						{	(user.id) ? 
								(user.isAdmin) ?  
											

								<>
								<Nav.Link as={NavLink} to="/admin">Admin</Nav.Link>
								<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
								</>

								
								:
								<>
								<Nav.Link as={NavLink} to="/products">Products</Nav.Link>
								<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
								</>


						:

						<>
						<Nav.Link as={NavLink} to="/products">Products</Nav.Link>
						<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
						<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
						</>

						}


					
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}