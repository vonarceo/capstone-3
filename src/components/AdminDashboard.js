import {useState, useEffect} from 'react'
import { Row, Col, Card, Modal, Form } from 'react-bootstrap'
import { Button, Table } from 'react-bootstrap'
import PropTypes from 'prop-types'
import { Link, useParams } from 'react-router-dom'
import Loading from '../components/Loading'
import Swal from 'sweetalert2'
import EditProductButton from './EditProductButton'
import DisableButton from './DisableButton'
import EnableButton from './EnableButton'

export default function AdminDashboard(){
        
  const {productId} = useParams()
  
  const [isLoading, setIsLoading] = useState(false)
  
  const [products, setProducts] = useState([])

  // const [updateName, setUpdateName] = useState('')
    // const [updateDescription, setUpdateDescription] = useState('')
    // const [updatePrice, setUpdatePrice] = useState('')
    // const [updateProductId, setUpdateProductId] = useState('')

    // const [show2, setShow2] = useState(false);
    // const handleClose2 = () => setShow2(false);
    // const handleShow2 = (productName, productDescription, productPrice, product_Id) => {
    //   setShow2(true)
    //   setUpdateName(productName)
    //   setUpdateDescription(productDescription)
    //   setUpdatePrice(productPrice)   
    //   setUpdateProductId(product_Id)
    // }; 


useEffect(() => {
// set the loading state to true 
  setIsLoading(true)

fetch(`${process.env.REACT_APP_API_URL}/products/all`)
.then(response => response.json())
.then(result => {
    setProducts(
      result.map(product => {
            
          return(
            <>  
              <tr>
                <td>{product._id}</td>
                <td>{product.name}</td>
                <td>{product.description}</td>
                <td>{product.price}</td>
                <td>{product.isActive ? 'Available' : 'Unavailable'}</td>

                { (product.isActive) ?
                <>
                <DisableButton product={product}/>
                
                </>
                :
                <>
                <EnableButton product={product}/>
                </>
                }

                <EditProductButton product={product}/>

              </tr> 
              
            </>
          )
          
        })
      )
    setIsLoading(false)
})
}, [])


    return(
        <>
        {/*<Modal show={show2} onHide={handleClose2}>
            <Modal.Header closeButton>
              <Modal.Title>Update Products</Modal.Title>
            </Modal.Header>
            
            <Modal.Body>
            
              
              <Form onSubmit={event => updateProduct(event)}>

                <Form.Group className="mb-1" controlId="name">
                  <Form.Label>Product Name:</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter Product Name"
                    value={updateName}
                    
                    onChange={event => setUpdateName(event.target.value)}
                    required
                  
                  />
                </Form.Group>
                

                <Form.Group
                  className="mb-1"
                  controlId="description"
                >
                  <Form.Label>Description:</Form.Label>
                  <Form.Control 
                    type="text"
                    placeholder="Enter Description"
                    value={updateDescription}
                    
                    onChange={event => setUpdateDescription(event.target.value)}
                    required    
                  />
                </Form.Group>

                <Form.Group
                  className="mb-1"
                  controlId="price"
                >
                  <Form.Label>Price:</Form.Label>
                  <Form.Control 
                    type="text"
                    placeholder="Enter Price"
                    value={updatePrice}
                    
                    onChange={event => setUpdatePrice(event.target.value)}
                    required    
                  />
                </Form.Group>

                <Button variant="danger" type="submit" id="submitBtn">
                    Update
                  </Button>

              </Form>
              

            </Modal.Body>
            
          </Modal>*/}  
         
         
        <Table variant="dark" striped bordered hover>
              <thead className="text-center">
                <tr>
                  <th>id#</th>
                  <th>Product Name</th>
                  <th>Description</th>
                  <th>Price</th>
                  <th>Status</th>
                  <th>Action</th>

                </tr>
              </thead>

              <tbody className="text-center">
                    {products}               
                
              </tbody>
            </Table> 
          </>
      )

}