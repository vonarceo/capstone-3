import {useState, useEffect} from 'react'
import { Button,  Modal, Form } from 'react-bootstrap'
import Swal from 'sweetalert2'
import {useNavigate, Navigate} from 'react-router-dom'


export default function EnableButton(product){
	
		const [DisableProductId, setDisableProductId] = useState(product.product._id)

		const [show4, setShow4] = useState(false);
		const handleClose4 = () => setShow4(false);
		const handleShow4 = () => setShow4(true);

		const navigate = useNavigate()
	 	

		function EnableProduct(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/${product.product._id}/archive2`,{
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json'
			}
		})
		.then(response => response.json())
		.then(result => {
			if(result){
				Swal.fire({
				  title: 'Success!',
				  icon: 'success',
				  text: 'Successfully Archive product!'
				})
				
			}
			else {
				Swal.fire({
				  title: 'Something went wrong',
				  icon: 'error',
				  text: 'please contact developer.'
				})
			}
		})
	}


	return(
		<>
		<Button className="m-1" variant="secondary" onClick={handleShow4}>Enable</Button>

		<Modal show={show4} onHide={handleClose4}>
		    <Modal.Header closeButton>
		      <Modal.Title>Archive a Product</Modal.Title>
		    </Modal.Header>
		    
		    <Modal.Body className="justify-content-center">
		    
		      
		      <Form onSubmit={event => EnableProduct(event)}>
		        
		        <Form.Label>Are you sure do you want to Enable the Product?</Form.Label>
		        <br/>
		          <Button variant="secondary" onClick={handleClose4}>
		            Cancel!
		          </Button>
		        <Button variant="danger" type="submit" onClick={handleClose4} id="submitBtn">
		            Enable!
		          </Button>

		      </Form>
		      

		    </Modal.Body>
		    
		  </Modal>
		</>
			
	)
}