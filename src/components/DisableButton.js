import {useState, useEffect} from 'react'
import { Button,  Modal, Form } from 'react-bootstrap'
import Swal from 'sweetalert2'


export default function DisableButton(product){
	
	
	const [DisableProductId, setDisableProductId] = useState(product.product._id)

	const [show3, setShow3] = useState(false);
	const handleClose3 = () => setShow3(false);
	const handleShow3 = () => setShow3(true);

	const refreshPage = ()=>{
	     window.location.reload();
	  }

	function DisableProduct(event){
	event.preventDefault()

	fetch(`${process.env.REACT_APP_API_URL}/products/${product.product._id}/archive`,{
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		}
	})
	.then(response => response.json())
	.then(result => {
		if(result){
			Swal.fire({
			  title: 'Success!',
			  icon: 'success',
			  text: 'Successfully Archive product!'
			})
		}
		else {
			Swal.fire({
			  title: 'Something went wrong',
			  icon: 'error',
			  text: 'please contact developer.'
			})
		}
	})
}





	return(
		<>
		<Button className="m-1" variant="secondary" onClick={handleShow3}>Disable</Button>

		<Modal show={show3} onHide={handleClose3}>
		    <Modal.Header closeButton>
		      <Modal.Title>Archive a Product</Modal.Title>
		    </Modal.Header>
		    
		    <Modal.Body className="justify-content-center">
		    
		      
		      <Form onSubmit={event => DisableProduct(event)}>
		        
		        <Form.Label>Are you sure do you want to Archive the Product?</Form.Label>
		        <br/>
		          <Button variant="secondary" onClick={handleClose3}>
		            Cancel!
		          </Button>
		        <Button variant="danger" type="submit" onClick={handleClose3} id="submitBtn">
		            Archive!
		          </Button>

		      </Form>
		      

		    </Modal.Body>
		    
		  </Modal>

		</>
		
)
}