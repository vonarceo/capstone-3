import { Row, Col, Card } from 'react-bootstrap'

export default function Highlights(){
	return(
		<Row className="mt-3 mb-3">
					            <Col xs={12} md={4}>
					                <Card className="cardHighlight p-3 shadow">
					                <Card.Img variant="top" src="../x1.PNG" />
					                    <Card.Body>
					                        <Card.Title>
					                            <h2>Shoes</h2>
					                        </Card.Title>
					                        <Card.Text id="card123">
					                            Pxariatur adipisicing aute do amet dolore cupidatat. Eu labore aliqua eiusmod commodo occaecat mollit ullamco labore minim. Minim irure fugiat anim ea sint consequat fugiat laboris id. Lorem elit irure mollit officia incididunt ea ullamco laboris excepteur amet. Cillum pariatur consequat adipisicing aute ex.
					                        </Card.Text>
					                    </Card.Body>
					                </Card>
					            </Col>
					            <Col xs={12} md={4}>
					                <Card className="cardHighlight p-3 shadow">
					                <Card.Img variant="top" src="../x4.PNG" />
					                    <Card.Body>
					                        <Card.Title>
					                            <h2>Apparels</h2>
					                        </Card.Title>
					                        <Card.Text id="card123">
					                            Ex Lorem cillum consequat ad. Consectetur enim sunt amet sit nulla dolor exercitation est pariatur aliquip minim. Commodo velit est in id anim deserunt ullamco sint aute amet. Adipisicing est Lorem aliquip anim occaecat consequat in magna nisi occaecat consequat et. Reprehenderit elit dolore sunt labore qui.
					                        </Card.Text>
					                    </Card.Body>
					                </Card>
					            </Col>
					            <Col xs={12} md={4}>
					                <Card className="cardHighlight p-3 shadow">
					                <Card.Img variant="top" src="../x2.jpg" />
					                    <Card.Body>
					                        <Card.Title>
					                            <h2>Accessories</h2>
					                        </Card.Title>
					                        <Card.Text id="card123">
					                            Minim nostrud dolore consequat ullamco minim aliqua tempor velit amet. Officia occaecat non cillum sit incididunt id pariatur. Mollit tempor laboris commodo anim mollit magna ea reprehenderit fugiat et reprehenderit tempor. Qui ea Lorem dolor in ad nisi anim. Culpa adipisicing enim et officia exercitation adipisicing.
					                        </Card.Text>
					                    </Card.Body>
					                </Card>
					            </Col>
					        </Row>		
	)
}