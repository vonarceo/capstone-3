import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext'
import {useParams, useNavigate, Link} from 'react-router-dom'
import Swal from 'sweetalert2'

export default function ProductView() {

	// Get courseId from the URL of the route that this component is connected to. '/courses/:courseId'
	const {productId} = useParams()

	const {user} = useContext(UserContext)

	const navigate = useNavigate()

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const checkOut = (productId) => {
		

		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				userId: user.id,
				productName: name

			})
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)
			if(result){
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Check-out successfully'
				})
				navigate('/products')
			} else {
				Swal.fire({
					title: 'Something went wrong!',
					icon: 'error',
					text: 'please try again :('
				})
			}
		})
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(response => response.json())
		.then(result => {
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
		})
	}, [productId])

	return(
		<Container className="mt-4">
			<Row>
				<Col lg={{ span: 4, offset: 4 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>₱{price}</Card.Text>
							

							{	user.id !== null ?
								<Button variant="danger" onClick={() => checkOut(productId)}>Buy</Button>
								:

								<Link className="btn btn-danger btn-block" to="/login">Login to buy</Link>
							}


							
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}