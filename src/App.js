import './App.css';
import {useState} from 'react'
import { UserProvider } from './UserContext'
import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';
import Admin from './pages/Admin'
import Home from './pages/Home'
import Dashboard from './components/Dashboard'
import Products from './pages/Products'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import ErrorPage from './pages/ErrorPage'
import { Container } from 'react-bootstrap'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'


function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    <>
  {/*provides the user context throughout any component inside of it */}
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container>
            <Routes>
              <Route path="/" element={<Home/>}/>
              <Route path="/products" element={<Products/>}/>
              <Route path="/products/:productId" element={<ProductView/>}/>
              <Route path="/login" element={<Login/>}/>
              
              <Route path="/register" element={<Register/>}/>
              <Route path="/logout" element={<Logout/>}/>
              <Route path="/admin" element={<Dashboard/>}/>
              <Route path="*" exact={true} element={<ErrorPage/>} />
                
            </Routes>
        </Container>
      </Router>  
    </UserProvider>
            {/*switching of component*/}
    
    
    </>
    
  );
}

export default App;

